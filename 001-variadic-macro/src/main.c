#include <stdio.h>

// Reference
// https://stackoverflow.com/questions/824639/variadic-recursive-preprocessor-macros-is-it-possible

#define ARGS_COUNT_(X,X64,X63,X62,X61,X60,X59,X58,X57,X56,X55,X54,X53,X52,X51,X50,X49,X48,X47,X46,X45,X44,X43,X42,X41,X40,X39,X38,X37,X36,X35,X34,X33,X32,X31,X30,X29,X28,X27,X26,X25,X24,X23,X22,X21,X20,X19,X18,X17,X16,X15,X14,X13,X12,X11,X10,X9,X8,X7,X6,X5,X4,X3,X2,X1,N,...) N
#define ARGS_COUNT(...) ARGS_COUNT_(0, __VA_ARGS__ ,64,63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0)

#define ARGS_INDEX_ODD_2(a, b) a
#define ARGS_INDEX_ODD_4(a, b, ...) a, ARGS_INDEX_ODD_2(__VA_ARGS__)
#define ARGS_INDEX_ODD_6(a, b, ...) a, ARGS_INDEX_ODD_4(__VA_ARGS__)
#define ARGS_INDEX_ODD_8(a, b, ...) a, ARGS_INDEX_ODD_6(__VA_ARGS__)
#define ARGS_INDEX_ODD_10(a, b, ...) a, ARGS_INDEX_ODD_8(__VA_ARGS__)
#define ARGS_INDEX_ODD_12(a, b, ...) a, ARGS_INDEX_ODD_10(__VA_ARGS__)
#define ARGS_INDEX_ODD_14(a, b, ...) a, ARGS_INDEX_ODD_12(__VA_ARGS__)
#define ARGS_INDEX_ODD_16(a, b, ...) a, ARGS_INDEX_ODD_14(__VA_ARGS__)
#define ARGS_INDEX_ODD_18(a, b, ...) a, ARGS_INDEX_ODD_16(__VA_ARGS__)
#define ARGS_INDEX_ODD_20(a, b, ...) a, ARGS_INDEX_ODD_18(__VA_ARGS__)
#define ARGS_INDEX_ODD_22(a, b, ...) a, ARGS_INDEX_ODD_20(__VA_ARGS__)
#define ARGS_INDEX_ODD_24(a, b, ...) a, ARGS_INDEX_ODD_22(__VA_ARGS__)

#define ARGS_INDEX_EVEN_2(a, b) b
#define ARGS_INDEX_EVEN_4(a, b, ...) b, ARGS_INDEX_EVEN_2(__VA_ARGS__)
#define ARGS_INDEX_EVEN_6(a, b, ...) b, ARGS_INDEX_EVEN_4(__VA_ARGS__)
#define ARGS_INDEX_EVEN_8(a, b, ...) b, ARGS_INDEX_EVEN_6(__VA_ARGS__)
#define ARGS_INDEX_EVEN_10(a, b, ...) b, ARGS_INDEX_EVEN_8(__VA_ARGS__)
#define ARGS_INDEX_EVEN_12(a, b, ...) b, ARGS_INDEX_EVEN_10(__VA_ARGS__)
#define ARGS_INDEX_EVEN_14(a, b, ...) b, ARGS_INDEX_EVEN_12(__VA_ARGS__)
#define ARGS_INDEX_EVEN_16(a, b, ...) b, ARGS_INDEX_EVEN_14(__VA_ARGS__)
#define ARGS_INDEX_EVEN_18(a, b, ...) b, ARGS_INDEX_EVEN_16(__VA_ARGS__)
#define ARGS_INDEX_EVEN_20(a, b, ...) b, ARGS_INDEX_EVEN_18(__VA_ARGS__)
#define ARGS_INDEX_EVEN_22(a, b, ...) b, ARGS_INDEX_EVEN_20(__VA_ARGS__)
#define ARGS_INDEX_EVEN_24(a, b, ...) b, ARGS_INDEX_EVEN_22(__VA_ARGS__)

#define ARGS_INDEX_ODD_N_(N, ...) ARGS_INDEX_ODD_ ## N(__VA_ARGS__)
#define ARGS_INDEX_ODD_N(N, ...) ARGS_INDEX_ODD_N_(N, __VA_ARGS__)
#define ARGS_INDEX_ODD(...) ARGS_INDEX_ODD_N(ARGS_COUNT(__VA_ARGS__), __VA_ARGS__)

#define ARGS_INDEX_EVEN_N_(N, ...) ARGS_INDEX_EVEN_ ## N(__VA_ARGS__)
#define ARGS_INDEX_EVEN_N(N, ...) ARGS_INDEX_EVEN_N_(N, __VA_ARGS__)
#define ARGS_INDEX_EVEN(...) ARGS_INDEX_EVEN_N(ARGS_COUNT(__VA_ARGS__), __VA_ARGS__)

int main()
{
  printf("%d %d %d\n", ARGS_INDEX_ODD(1, 2, 3, 4, 5, 6));
  printf("%d %d %d\n", ARGS_INDEX_EVEN(1, 2, 3, 4, 5, 6));

  return 0;
}
